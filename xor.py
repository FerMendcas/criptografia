def cipher(message):
    """
    Cifra el mensaje recibido como parámetro con el algoritmo de cifrado
    XOR.
    Parámetro:
       message -- el mensaje a cifrar.
    """
    msg_len = len(message)  # La longitud ayuda para repetir la llave.
    cif = ""
    for i in range(msg_len):  # Recorremos el mensaje.
        cif = cif + chr(ord(message[i]) ^ 1) # Le aplicamos el xor al entero de su unicode y lo regresamos como caracter.
    return cif
                

def decipher(criptotext):
    """
    Descifra el mensaje recuperando el texto plano siempre y cuando haya
    sido cifrado con XOR.
    Parámetro:
       cryptotext -- el mensaje a descifrar.
    """

    #Funciona igual para descifrar.
    ctext_len = len(criptotext)
    dcif = ""
    for i in range(ctext_len):
        dcif = dcif + chr(ord(criptotext[i]) ^ 1)
    return dcif
