import random

class Vigenere():

    def __init__(self, alphabet, password=None):
        #Recomendación, ingeniárselas para no cargar siempre O(n^2) en memoria aunque esto no
        #será evaluado, con n el tamaño del alfabeto.
        """
        Constructor de clase, recibe un parámetro obligatorio correspondiente al alfabeto
        y un parámetro opcional que es la palabra clave, en caso de ser None, entonces
        generar una palabra pseudoaleatoria de al menos tamaño 4.
        :param alphabet: Alfabeto a trabajar con el cifrado.
        :param password: El password que puede ser o no dada por el usuario.
        """
        self.alphabet = alphabet
        if password is None:  #Si no se recibe una contraseña.
            pass_len = random.randint(4, 50)  #Se fijo un máximo por practicidad.
            for i in range(pass_len):
                self.password = str(password) + random.choice(self.alphabet)  #Se elige al azar del alfabeto.
        else:
            self.password = password  #Si recibimos la contraseña.


    def cipher(self, message):  #Ocupamos la fórmula mensaje[i] + llave[i] (mod len(alfabeto)).
        """
        Usando el algoritmo de cifrado de vigenere, cifrar el mensaje recibido como parámetro,
        usando la tabla descrita en el PDF.
        :param message: El mensaje a cifrar.
        :return: Una cadena de texto con el mensaje cifrado.
        """
        str_ciph = ""
        mess_len = len(message)  #Longitud del mensaje.
        pass_adec = (self.password * mess_len)[:mess_len]  #Ajustamos el tamaño de la contraseña al 
                                                                #del mensaje.
        for i in range(mess_len):
            mess_ind = self.alphabet.find(message[i]) #Índice de la letra del mensaje en el alfabeto.
            pass_ind = self.alphabet.find(pass_adec[i])  #Índice de la letra de la llave en el alfabeto.
            sum_ind = (mess_ind + pass_ind) % len(self.alphabet)  #Suma de índices.
            str_ciph = str_ciph + self.alphabet[sum_ind]  #Agregamos la letra del índice.
        return str_ciph


    def decipher(self, ciphered):  #Ocupamos la fórmula criptotexto[i] - llave[i] (mod len(alfabeto)).
        """
        Implementación del algoritmo de decifrado, según el criptosistema de vigenere.
        :param ciphered: El criptotexto a decifrar.
        :return: El texto plano correspondiente del parámetro recibido.
        """
        #Similar a cipher().
        str_deciph = ""
        ciph_len = len(ciphered)
        pass_adec = (self.password * ciph_len)[:ciph_len]
        for i in range(ciph_len):
            ciph_ind = self.alphabet.find(ciphered[i])
            pass_ind = self.alphabet.find(pass_adec[i])
            sub_ind = (ciph_ind - pass_ind) % len(self.alphabet)  #Resta de índices.
            str_deciph = str_deciph + self.alphabet[sub_ind]
        return str_deciph

a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
v = Vigenere(a, "so")
m = "﻿SZHDY IAGJO KXGEQ EBOSD MCBAE OECYAYQMSR VUNGE ESJRA URDWX FRHGE ALOTXHSGAF HVJXO EOSEL RYNZI SAFAG AYTJOKJGAY TJOKX UQHIW BDRUM BRTIJ LUCBNKHRWN ENLII VCLAG OVSRV IRUAD ZFMCAZAFHV HMOLD AODJH TRADE LNXEN MGFDNNRNTS HIRON SAFHV ZOIRR GJVAU LDUWAVVUTN YEOKX ULDUW AVZOI RRWJV ANRVUFTRWN EFISS JCYPN GGDRM YCRMA RVBYOFUJIV PIANQ MSCVC PHNVC EXLEA OBCUNYNGRS ASXMM BDGGZ WZEYI RAVEY OCEJCPXJOE MWXFA JAETA RFNMC BJGRV ZOIRNFCHDC EEOKS IECOY EFHFN GPYEG ELNXEDUASE WIMRQ MWVAY VVLVS JYIJB JMOEJLAZIJ SQMYA FBSXV".replace(" ", "")
print(m)
print(v.decipher(m))
#SZHDYIAGJOKXGEQEBOSDMCBAEOECYAYQMSRVUNGEESJRAURDWXFRHGEALOTXHSGAFHVJXOEOSELRYNZISAFAGAYTJOKJGAYTJOKXUQHIWBDRUMBRTIJLUCBNKHRWNENLIIVCLAGOVSRVIRUADZFMCAZAFHVHMOLDAODJHTRADELNXENMGFDNNRNTSHIRONSAFHVZOIRRGJVAULDUWAVVUTNYEOKXULDUWAVZOIRRWJVANRVUFTRWNEFISSJCYPNGGDRMYCRMARVBYOFUJIVPIANQMSCVCPHNVCEXLEAOBCUNYNGRSASXMMBDGGZWZEYIRAVEYOCEJCPXJOEMWXFAJAETARFNMCBJGRVZOIRNFCHDCEEOKSIECOYEFHFNGPYEGELNXEDUASEWIMRQMWVAYVVLVSJYIJBJMOEJLAZIJSQMYAFBSXV
