from random import randrange 

def big_int(size=None):
    """
    Generador de números aleatorios de un tamaño fijo recibido como parámetro, si el parámetro es
    menor que 100 o None, entonces la función no le hace caso y genera uno de tamaño arbitrario,
    máximo es de 150 dígitos.
    :return: Un número del tamaño descrito.
    """
    num = ""
    if (size == None or size < 100):
        new_size = randrange(100, 150)
        for i in range(new_size):
            num += str(randrange(0,9))
        return int(num)
    else:
        for i in range(size):
            num += str(randrange(0,9))
        return int(num)



def wilson(n):
    """
    Implementación del test de primalidad de Wilson, basado en el teorema de Wilson,
    (p-1)! ≡ -1 mod p
    :param n: El número a determinar su primalidad.
    :return: True si n es primo, False en otro caso.
    """
    #Veamoslo como (n - 1)! mod n  = (n - 1) si n es primo.
    prev = n - 1 #Número con el que verificamos la igualdad.
    if(factorial(prev, n) == prev): # prev! % n = prev
        return True 
    else:
        return False


def factorial(n, m): 
    """
    Función auxiliar para obtener el modulo del factorial.
    :param n: Número al que se le sacara el factorial.
    :param m: Módulo.
    :return: n! % m
    """
    if n >= p: # Vemos que n no sea mas grande que p.
        return 0    
    fac = 1
    for i in range(1, n + 1): # Vemos que usemos todos los números anteriores.
        fac = (fac * i) % m  # Factorial como lo conocemos módulo m.
    return fac
