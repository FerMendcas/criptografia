from utils import my_inverso
from utils import perfecto

#Para todo el código seguimos al pie de la letra la presentacion y
#el video de classroom.

class Curve():

    def __init__(self, A, B, p):
        """
        Construcutor de clase que va a representar a la curva elíptica de la
        forma y^2 = x^3 + Ax + B (mod p).
        :param A: primer coeficiente de la curva.
        :param B: segundo coeficiente de la curva.
        :param p: el tamaño del campo sobre el cual se hace la curva.
        """
        self.A=A
        self.B=B
        self.p=p

    def is_on_curve(self, point):
        """
        Método de clase regresa true si un punto está en la curva, éste punto
        está representado a manera de tupla, como (x, y).
        :param point: Una tupla de enteros representando a un punto.
        :return: true si el punto está en la curva, false en otro caso.
        """
        if point==None:
            return True
        A,B,p=self.A,self.B,self.p
        x,y=point
        return (y**2)%p == (x**3 + A*x + B)%p

    def determinant(self):
        """
        Regresa el determinante de una curva, recordando que su determinante
        es calculado de la forma 4A^3 + 27B^2.
        :return: El entero con el valor del determinante.
        """
        B,A=self.B, self.A
        return (4*A**3) + (27*B**2)

def add_points(p, q, curve):
    """
    Dados un par de puntos y una curva elíptica, calcula el punto de la suma
    bajo la curva elíptica recibida como parámetro, se asume que p y q ya
    forman parte de la curva.
    :param p: una tupla representando un punto de la curva.
    :param q: una tupla representando un punto de la curva.
    :param curve: una instancia de la clase de este script.
    :return: Una tupla que contiene el resultado de la suma o None en caso de
    que haya sido evaluada al punto infinito.
    """
    x1,y1=p
    x2,y2=q
    A,pM=curve.A,curve.p
    if x1 == x2 and pM-y1 == y2:
        return None
    if p==q:
        m1= 3*x1**2
        m2=my_inverso(2*y1,pM)
        l= ((m1 + A) * m2) % pM
    else:
        l=((y1-y2) * my_inverso(x1-x2,pM))%pM
    x3= (l**2-x1-x2)%pM
    return x3, ((l*(x1-x3))-y1)%pM


#Encontramos el articulo donde viene como hacer el punto extra en
# https://www.idc-online.com/technical_references/pdfs/data_communications/Point%20Multiplication.pdf
def scalar_multiplication(p, k, curve):
    """
    Dado un escalar del campo, k, calcula el punto kP bajo la definición
    de curvas elípticas.
    Usamos la idea del artículo citado arriba sólo sin considerar la
    funcion Point Doubling porque en add_points ya consideramos el caso
    en el que los dos puntos sean iguales.
    :param p: una tupla representando un punto de la curva.
    :param k: el escalar a multiplicar por el punto.
    :param curve: la curva sobre la cual se calculan las operaciones.
    :return: una tupla representando a kP o None si al sumar ese punto cayó
    en algún momento al punto infinito.
    """
    if p== None:
        return None
    if k == 1:
        return p
    if k & 1 :
        return add_points(p, scalar_multiplication(p, k-1,curve),curve)
    else:
        return scalar_multiplication(add_points(p,p,curve), k//2,curve)

