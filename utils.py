import math
def prime_relative(a, b):
    if(b == 0):
        return a == 1
    else:
        return prime_relative(b, a%b)

def my_inverso(a, m) :
    """
        Regresa el inverso modular de a respecto de m, ya que para decifrar hay
        que aplicar la funcion D(c) = (alphabet[c]-b)*a^-1, donde a^-1 es el
        inverso modular, ya que se despeja de la funcion E(c)a= a*alphabet[c]+b
            a -- factor A de la llave.
            m -- longitud del alfabeto sobre el cual estamos ciifrando.
    """
    #Primero sacamos el modulo de a respecto a m para "reducir"
    a = a % m
    #Ahora para cada número desde el 1 hasta m revisamos si dicho número es el
    #inverso que buscamos.
    for x in range(1, m) :
        if ((a * x) % m == 1) :
            return x
    #En caso de no existir el inverso modular regresamos 1
    return 1

def perfecto(n):
    """
        Funcion que dado un número n responde si el número tiene
        raíz cuadrada entera.
    """
    a = int(math.sqrt(n))
    return a**2==n

