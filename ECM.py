import EllipticCurves
import utils
from random import randrange
from math import gcd

def lenstra(n):
    """
    Implementación del algoritmo de Lenstra para encontrar los factores
    primos de un número n de la forma n = p*q. Se asume que la proposición
    anterior es cierta, es decir, que en efecto n = p*q, regresando ambos
    factores de n.
    """
    curve = EllipticCurves.Curve(randrange(0, n + 1), randrange(0, n + 1), n) # Curva aleatoria.
    while not utils.prime_relative(curve.determinant(), n): # ¿Es una buena curva?
    	curve = EllipticCurves.Curve(randrange(0, n + 1), randrange(0, n + 1), n)
    point = (randrange(1, n + 1), randrange(1, n + 1)) # Punto aleatorio.
    while not curve.is_on_curve(point): # ¿Está en la curva?
    	point = (randrange(1, n + 1), randrange(1, n + 1))
    point_aux = point # El punto que lleva la suma.
    p = 2
    pp = 1
    while utils.my_inverso(point_aux[1], n) != 1: # Hasta que no tenga inverso.
    	point_aux = EllipticCurves.add_points(point, point_aux, curve) # Sumamos a el mismo.
    	p += 1 # Las veces que sumamos.
    if(n < 100): # Con este metodo funciona para numeros pequeños
        if(p >= 4): # Restamos lo que agregamos para no pasarnos en el menor caso.
            q = n // p - 2 #
        else:
            q = n // p # Si no, solo dividimos
        #print(p, q)
        return (p, q)
    else:
        pp = gcd(n, point_aux[1]) #Método por el maximo común divisor entre el
                                    #inverso y n.
        q = n // pp
        #print(pp,q)
        return (pp, q)    
    

#lenstra(6)
#lenstra(130063)